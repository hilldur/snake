public class TurnPoint {
    int x;
    int y;
    int dir;

    public TurnPoint(int x, int y, int dir) {
        this.x = x;
        this.y = y;
        this.dir = dir;
    }
}

public class CONSTANTS {
    final static int SNAKE_SPEED = 4;
    final static int SNAKE_RADIUS = 10;
    final static int APPLE_RADIUS = 10;

    final static int MAX_APPLE_AMOUNT = 5;

    final static int SNAKE_RADIUS_COMPARE = SNAKE_RADIUS * SNAKE_RADIUS * 2;
    final static int APPLE_RADIUS_COMPARE = APPLE_RADIUS * APPLE_RADIUS * 4;

    final static int WINDOW_WIDTH = 500;
    final static int WINDOW_HEIGHT = 500;

    final static int TEXTBOX_WIDTH = 100;
    final static int TEXTBOX_HEIGHT = 50;

    final static int ENDGAME_TEXTBOX_WIDTH = WINDOW_WIDTH / 2;
    final static int ENDGAME_TEXTBOX_HEIGHT = WINDOW_HEIGHT / 3;

    final static int SHRINK_APPLE_CHANCE = 15;

    final static int WIN_THRESHOLD = 100;
}

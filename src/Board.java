import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;

public class Board extends JPanel {
    Snake snake;
    AppleHandler appleHandler;
    JLabel scoreLabel;
    JLabel highscoreLabel;
    int score = 0;
    int highscore = 0;
    boolean win = false;
    boolean loose = false;
    boolean paused = false;

    public Board() {
        this.snake = new Snake();
        this.appleHandler = new AppleHandler();
        scoreLabel = new JLabel("Score: " + score);
        scoreLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
        scoreLabel.setBounds(CONSTANTS.WINDOW_WIDTH / 2, 0, CONSTANTS.TEXTBOX_WIDTH, CONSTANTS.TEXTBOX_HEIGHT);
        this.add(scoreLabel);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(CONSTANTS.WINDOW_WIDTH, CONSTANTS.WINDOW_HEIGHT);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (!win && !loose) {
            this.setBackground(new Color(26, 172, 45));
            this.snake.draw(g);
            scoreLabel.setText("Score: " + score);
            this.appleHandler.spawnApple(this.snake.getPoints());
            this.appleHandler.getApples().forEach((apple) -> {
                apple.draw(g);
            });
        } else if (loose) {
            this.setBackground(Color.RED);
            JLabel gameOverLabel = new JLabel("Game Over!", SwingConstants.CENTER);
            gameOverLabel.setBounds((CONSTANTS.WINDOW_WIDTH / 2) - (CONSTANTS.ENDGAME_TEXTBOX_WIDTH / 2),
                    (CONSTANTS.WINDOW_HEIGHT / 2) - (CONSTANTS.ENDGAME_TEXTBOX_HEIGHT / 2),
                    CONSTANTS.ENDGAME_TEXTBOX_WIDTH,
                    CONSTANTS.ENDGAME_TEXTBOX_HEIGHT);
            gameOverLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 32));
            this.add(gameOverLabel);
        } else if (win) {
            this.setBackground(Color.GREEN);
            JLabel winLabel = new JLabel("You win!", SwingConstants.CENTER);
            winLabel.setBounds((CONSTANTS.WINDOW_WIDTH / 2) - (CONSTANTS.ENDGAME_TEXTBOX_WIDTH / 2),
                    (CONSTANTS.WINDOW_HEIGHT / 2) - (CONSTANTS.ENDGAME_TEXTBOX_HEIGHT / 2),
                    CONSTANTS.ENDGAME_TEXTBOX_WIDTH,
                    CONSTANTS.ENDGAME_TEXTBOX_HEIGHT);
            winLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 32));
            this.add(winLabel);
        }
    }

    public void createHighscoreLabel(int highscore) {
        this.highscore = highscore;
        highscoreLabel = new JLabel("Highscore: " + highscore);
        highscoreLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
        highscoreLabel.setBounds(0, 0, CONSTANTS.TEXTBOX_WIDTH, CONSTANTS.TEXTBOX_HEIGHT);
        this.add(highscoreLabel);
    }

    public void incrementScore() {
        this.score++;
    }

    public void changeDirection(int dir) {
        this.snake.changeDirection(dir);
    }

    public void createTail() {
        this.snake.createTail();
    }

    public void shrinkTail() {
        this.snake.shrinkTail();
    }

    public ArrayList<Point> getSnake() {
        return this.snake.getPoints();
    }

    public ArrayList<Apple> getApples() {
        return this.appleHandler.getApples();
    }

    public void gameOver() {
        this.loose = true;
    }

    public void win() {
        this.win = true;
    }

    public void pause() {
        this.paused = true;
    }

    public void resume() {
        this.paused = false;
    }
}

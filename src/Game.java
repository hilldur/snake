
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.*;
import javax.swing.JFrame;

public class Game extends JFrame implements KeyListener {
    CollisionHandler collisionHandler;
    Board board;
    Timer timer = new Timer();
    int score = 0;
    int highscore = 0;
    boolean timerIsRunning = false;

    public void start() {
        setup();
    }

    public void setup() {
        collisionHandler = new CollisionHandler();
        board = new Board();
        this.highscore = getHighscoreFromFile();
        this.add(board);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(board);
        this.pack();
        this.setVisible(true);
        this.addKeyListener(this);
        this.board.createHighscoreLabel(highscore);
        TimerTask gameloop = new TimerTask() {
            public void run() {
                gameloop();
            }
        };
        timer.scheduleAtFixedRate(gameloop, 0, 20);
        this.timerIsRunning = true;
        pause();
    }

    private void eatApple(Apple apple) {
        this.board.createTail();
        this.board.appleHandler.eatApple(apple);
    }

    private void eatShrinkApple(Apple apple) {
        this.board.shrinkTail();
        this.board.appleHandler.eatApple(apple);
    }

    private void gameloop() {
        Apple apple = this.collisionHandler.checkAppleCollision(this.board.getSnake().get(0), this.board.getApples());
        if (apple != null) {
            if (apple.getType() == 0) {
                eatApple(apple);
            } else {
                eatShrinkApple(apple);
            }
            this.board.incrementScore();
        }
        ;
        if (this.collisionHandler.checkSnakeCollision(this.board.getSnake())) {
            createFile();
            pause();
            this.board.gameOver();
        }
        ;
        if (this.board.score == CONSTANTS.WIN_THRESHOLD) {
            createFile();
            pause();
            this.board.win();
        }
        this.board.repaint();
    }

    public void createFile() {
        try {
            File myObj = new File("highscores.txt");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            }
            writeToFile();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void writeToFile() {
        try {
            FileWriter myWriter = new FileWriter("highscores.txt", true);
            myWriter.write("Score: " + this.board.score + "\n");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public int getHighscoreFromFile() {
        int highscore = 0;
        try {
            Scanner scanner = new Scanner(new File("highscores.txt"));
            while (scanner.hasNextLine()) {
                String[] highscores = scanner.nextLine().split(":");

                if (Integer.parseInt(highscores[1].trim()) > highscore) {
                    highscore = Integer.parseInt(highscores[1].trim());
                }

            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return highscore;
    }

    public void pause() {
        if (this.board.paused) {
            this.board.resume();
            resume();
        } else {
            this.timerIsRunning = false;
            this.timer.cancel();
            this.board.pause();
        }
    }

    public void resume() {
        this.timerIsRunning = true;
        timer.cancel();
        this.timer = new Timer();
        TimerTask gameloop = new TimerTask() {
            public void run() {
                gameloop();
            }
        };
        timer.scheduleAtFixedRate(gameloop, 0, 20);
    }

    public void restart() {
        setup();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 32) {
            pause();
        } else if (e.getKeyCode() == 82) {
            restart();
        }
        board.changeDirection(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}

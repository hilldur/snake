import java.util.ArrayList;

public class CollisionHandler {
    public Apple checkAppleCollision(Point snakeHead, ArrayList<Apple> apples) {
        for (int i = 0; i < apples.size(); i++) {
            if ((apples.get(i).getX() - snakeHead.getX()) * (apples.get(i).getX() - snakeHead.getX())
                    + (apples.get(i).getY() - snakeHead.getY())
                            * (apples.get(i).getY() - snakeHead.getY()) < CONSTANTS.APPLE_RADIUS_COMPARE) {
                return apples.get(i);
            }
        }
        return null;
    }

    public boolean checkSnakeCollision(ArrayList<Point> snakePoints) {
        Point snakeHead = snakePoints.get(0);
        for (int i = 1; i < snakePoints.size(); i++) {
            if ((snakePoints.get(i).getX() - snakeHead.getX()) * (snakePoints.get(i).getX() - snakeHead.getX())
                    + (snakePoints.get(i).getY() - snakeHead.getY())
                            * (snakePoints.get(i).getY() - snakeHead.getY()) < CONSTANTS.SNAKE_RADIUS_COMPARE) {
                return true;
            }
        }

        if (snakeHead.getX() + CONSTANTS.SNAKE_RADIUS * 2 >= CONSTANTS.WINDOW_WIDTH) {
            return true;
        } else if (snakeHead.getY() + CONSTANTS.SNAKE_RADIUS * 2 >= CONSTANTS.WINDOW_HEIGHT) {
            return true;
        } else if (snakeHead.getX() + CONSTANTS.SNAKE_RADIUS <= CONSTANTS.SNAKE_RADIUS) {
            return true;
        } else if (snakeHead.getY() + CONSTANTS.SNAKE_RADIUS <= CONSTANTS.SNAKE_RADIUS) {
            return true;
        }
        return false;
    }
}

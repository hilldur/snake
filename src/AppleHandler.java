import java.util.ArrayList;

public class AppleHandler {

    ArrayList<Apple> apples;

    public AppleHandler() {
        this.apples = new ArrayList<Apple>();
    }

    public void spawnApple(ArrayList<Point> occupiedPoints) {
        int width = CONSTANTS.WINDOW_WIDTH - (CONSTANTS.APPLE_RADIUS * 4);
        int height = CONSTANTS.WINDOW_HEIGHT - (CONSTANTS.APPLE_RADIUS * 4);

        if (apples.size() < CONSTANTS.MAX_APPLE_AMOUNT) {
            double tempX = Math.random() * width
                    + CONSTANTS.APPLE_RADIUS;

            double tempY = Math.random() * height
                    + CONSTANTS.APPLE_RADIUS;

            boolean legal = true;
            for (int i = 0; i < occupiedPoints.size(); i++) {
                if ((occupiedPoints.get(i).getX() - tempX) * (occupiedPoints.get(i).getX() - tempX)
                        + (occupiedPoints.get(i).getY() - tempY)
                                * (occupiedPoints.get(i).getY() - tempY) < CONSTANTS.APPLE_RADIUS_COMPARE) {
                    legal = false;
                }
            }
            for (int i = 0; i < apples.size(); i++) {
                if ((apples.get(i).getX() - tempX) * (apples.get(i).getX() - tempX)
                        + (apples.get(i).getY() - tempY)
                                * (apples.get(i).getY() - tempY) < CONSTANTS.APPLE_RADIUS_COMPARE) {
                    legal = false;
                }
            }

            if (legal) {
                double shrinkChance = Math.random() * 100 + 1;
                if (shrinkChance > CONSTANTS.SHRINK_APPLE_CHANCE) {
                    apples.add(new Apple((int) tempX, (int) tempY, 0));
                } else {
                    apples.add(new Apple((int) tempX, (int) tempY, 1));
                }
            }
        }
    }

    public void eatApple(Apple apple) {
        apples.remove(apple);
    }

    public ArrayList<Apple> getApples() {
        return apples;
    }

    public void setApples(ArrayList<Apple> apples) {
        this.apples = new ArrayList<Apple>(apples);
    }
}

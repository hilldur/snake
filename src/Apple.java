import java.awt.Graphics;
import java.awt.Color;

public class Apple implements Drawable {
    int x;
    int y;
    int radius = CONSTANTS.APPLE_RADIUS;
    int type;

    public Apple(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    @Override
    public void draw(Graphics g) {
        if (type == 0) {
            g.setColor(Color.RED);

        } else {
            g.setColor(Color.YELLOW);
        }
        g.fillOval(x, y, 2 * radius, 2 * radius);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getType() {
        return type;
    }

    public int getRadius() {
        return radius;
    }
}

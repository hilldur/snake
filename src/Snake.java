import java.util.ArrayList;
import java.awt.Color;
import java.awt.Graphics;

public class Snake implements Drawable {
    ArrayList<Point> points;
    ArrayList<Point> turnPoints;
    int length;
    int radius = CONSTANTS.SNAKE_RADIUS;

    public Snake() {
        this.points = new ArrayList<Point>();
        points.add(new Point(CONSTANTS.WINDOW_WIDTH / 2, CONSTANTS.WINDOW_HEIGHT / 2, 39));
        points.add(
                new Point(CONSTANTS.WINDOW_WIDTH / 2 - (CONSTANTS.SNAKE_RADIUS * 2), CONSTANTS.WINDOW_HEIGHT / 2, 39));
        points.add(new Point(CONSTANTS.WINDOW_WIDTH / 2 - (CONSTANTS.SNAKE_RADIUS * 2) * 2, CONSTANTS.WINDOW_HEIGHT / 2,
                39));
        turnPoints = new ArrayList<Point>();
    }

    public void draw(Graphics g) {

        for (int i = 0; i < points.size(); i++) {
            for (int y = 0; y < turnPoints.size(); y++) {
                if ((points.get(i).getX() == turnPoints.get(y).getX()
                        && points.get(i).getY() == turnPoints.get(y).getY())) {
                    if (i == points.size() - 1) {
                        points.get(i).setDir(turnPoints.get(y).getDir());
                        turnPoints.remove(y);
                    } else {
                        points.get(i).setDir(turnPoints.get(y).getDir());
                    }
                }
            }
            switch (points.get(i).getDir()) {
                case 39:
                    points.get(i).x += CONSTANTS.SNAKE_SPEED;
                    break;
                case 38:
                    points.get(i).y -= CONSTANTS.SNAKE_SPEED;
                    break;
                case 40:
                    points.get(i).y += CONSTANTS.SNAKE_SPEED;
                    break;
                case 37:
                    points.get(i).x -= CONSTANTS.SNAKE_SPEED;
                    break;
            }
            if (i % 2 == 0) {
                g.setColor(Color.MAGENTA);
            } else {
                g.setColor(Color.PINK);
            }
            g.fillOval(points.get(i).getX(), points.get(i).getY(), radius * 2, radius * 2);
        }

    }

    public void createTail() {
        int x = points.get(points.size() - 1).getX();
        int y = points.get(points.size() - 1).getY();
        int dir = points.get(points.size() - 1).getDir();
        switch (dir) {
            case 39:
                points.add(new Point(x - 20, y, dir));
                break;
            case 38:
                points.add(new Point(x, y + 20, dir));
                break;
            case 40:
                points.add(new Point(x, y - 20, dir));
                break;
            case 37:
                points.add(new Point(x + 20, y, dir));
                break;
        }
    }

    public void shrinkTail() {
        if (points.size() > 1) {
            points.remove(points.size() - 1);
        }
    }

    public ArrayList<Point> getPoints() {
        return this.points;
    }

    public void changeDirection(int dir) {
        if (dir >= 37 && dir <= 40) {
            if (!(points.get(0).dir == 37 && dir == 39)
                    && !(points.get(0).dir == 39 && dir == 37)
                    && !(points.get(0).dir == 38 && dir == 40)
                    && !(points.get(0).dir == 40 && dir == 38)) {
                points.get(0).dir = dir;
                turnPoints.add(new Point(points.get(0).getX(), points.get(0).getY(), dir));
            }
        }
    }

    public int getRadius() {
        return radius;
    }
}
